const express = require('express');
const app = express()
const port = 9000;

const middleware = (req, res, next) => {
    console.log("Hello from Middleware..")
    next();
}

// create route
app.get('/',(req, res)=>{
    res.send('Info1 => Hello World from Server..')
});

app.get('/userLogin',middleware,(req, res)=>{
    res.send('User login')
    console.log("Hello from userLogin..")
    
});

app.get('/userRegister',(req, res)=>{
    res.send('User Register')
});


app.listen(port, () => { console.log(`Server listen to localhost:${port}`)});